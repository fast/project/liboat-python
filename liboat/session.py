#!/usr/bin/env python3
import requests
import base64
import jwt          # https://pyjwt.readthedocs.io/en/latest/index.html
from datetime import datetime
import time
from threading import RLock
import random


# From http://docs.python-requests.org/en/master/, the library that this builds on:
# The use of Python 3 is highly preferred over Python 2. Consider upgrading your
# applications and infrastructure if you find yourself still using Python 2 in
# production today. If you are using Python 3, congratulations — you are indeed
# a person of excellent taste.
# —Kenneth Reitz

#try:
from collections import OrderedDict
#except ImportError:
    # Python < 2.7
#    from ordereddict import OrderedDict

#try:
from datetime import timezone
#except ImportError:
#    import pytz as timezone


# ======================================================================
'''
Dictionary with auto-expiring values for caching purposes.

Expiration happens on any access, object is locked during cleanup from expired
values. Can not store more than max_len elements - the oldest will be deleted.

>>> ExpiringDict(max_len=1000, max_age_seconds=2000, age_random_gitter=30)

The values stored in the following way:
{
    key1: (value1, created_time1),
    key2: (value2, created_time2)
}
NOTE: iteration over dict and also keys() do not remove expired values!
'''


class ExpiringDict(OrderedDict):
    def __init__(self, max_len, max_age_seconds, age_random_gitter):
        OrderedDict.__init__(self)
        self.max_len = max_len
        self.max_age = max_age_seconds
        self.age_random_gitter = age_random_gitter
        self.lock = RLock()

    def __contains__(self, key):
        '''
        Return True if the dict has a key, else return False.\
        '''
        try:
            with self.lock:
                item = OrderedDict.__getitem__(self, key)
                if time.time() - item[1] < self.max_age:
                    return True
                else:
                    del self[key]
        except KeyError:
            pass
        return False

    def __getitem__(self, key, with_age=False):
        '''
        Return the item of the dict.

        Raises a KeyError if key is not in the map.
        '''
        with self.lock:
            item = OrderedDict.__getitem__(self, key)
            item_age = time.time() - item[1]
            if item_age < self.max_age:
                if with_age:
                    return item[0], item_age
                else:
                    # TODO: deep copy needed?
                    return item[0]
            else:
                del self[key]
                raise KeyError(key)

    def __setitem__(self, key, value):
        """ Set d[key] to value. """
        with self.lock:
            if len(self) == self.max_len:
                self.popitem(last=False)
            rnd_gitter = 0
            if self.age_random_gitter > 0:
                rnd_gitter = random.randint(0, self.age_random_gitter)
            OrderedDict.__setitem__(self, key, (value, time.time() + rnd_gitter))

    def pop(self, key, default=None):
        '''
        Get item from the dict and remove it.

        Return default if expired or does not exist. Never raise KeyError.
        '''
        with self.lock:
            try:
                item = OrderedDict.__getitem__(self, key)
                del self[key]
                return item[0]
            except KeyError:
                return default

    def ttl(self, key):
        '''
        Return TTL of the `key` (in seconds).

        Returns None for non-existent or expired keys.
        '''
        key_value, key_age = self.get(key, with_age=True)
        if key_age:
            key_ttl = self.max_age - key_age
            if key_ttl > 0:
                return key_ttl
        return None

    def get(self, key, default=None, with_age=False):
        '''
        Return the value for key if key is in the dictionary, else default.
        '''
        try:
            return self.__getitem__(key, with_age)
        except KeyError:
            if with_age:
                return default, None
            else:
                return default

    def items(self):
        '''
        Return a copy of the dictionary's list of (key, value) pairs.
        '''
        r = []
        for key in self:
            try:
                r.append((key, self[key]))
            except KeyError:
                pass
        return r

    def values(self):
        '''
        Return a copy of the dictionary's list of values.
        See the note for dict.items().
        '''
        r = []
        for key in self:
            try:
                r.append(self[key])
            except KeyError:
                pass
        return r

    def fromkeys(self):
        raise NotImplementedError()

    def iteritems(self):
        raise NotImplementedError()

    def itervalues(self):
        raise NotImplementedError()

    def viewitems(self):
        raise NotImplementedError()

    def viewkeys(self):
        raise NotImplementedError()

    def viewvalues(self):
        raise NotImplementedError()


class OATException(Exception):
    """An error generated by OAT."""
    def __init__(self, jsonErr):
        # print(jsonErr)
        self.error = jsonErr

# ======================================================================


class Session:
    api_user = None
    api_key = None
    api_url = None
    api_auth_url = None
    api_headers = None
    session = None
    session_request = None
    cache = None
    max_retries = 3
    verify_ssl = True

    def __init__(
        self,
        api_user,
        api_key,
        api_url='https://oat.uwaterloo.ca',
        api_headers=None,
        cache_max_len=2000,
        cache_max_age_seconds=3600, verify_ssl=True
    ):
        self.api_user = api_user
        self.api_key = api_key
        self.api_url = api_url
        self.api_auth_url = '{0}/api/v2/authenticate'.format(api_url)
        self.api_headers = api_headers
        self.cache = ExpiringDict(
            max_len=cache_max_len,
            max_age_seconds=cache_max_age_seconds,
            age_random_gitter=120
        )
        self.verify_ssl = verify_ssl
        self.auth_token = ""

    def get_credential(self):
        '''
        return a credential object given a client_id and key
        '''
        # print("iat: ", datetime.now(timezone.utc))
        # print("iat: ", datetime.utcnow())
        jwt_token = jwt.encode(
            {
                'iss': __file__,
                'sub': self.api_user,
                'iat': datetime.utcnow()
            },
            self.api_key,
            algorithm='RS256'
        )
        return jwt_token

    def _create_session(self):
        '''
        create new session
        '''
        s = requests.session()
        if self.api_headers:
            s.headers = self.api_headers
        self.session_request = s.post(
            self.api_auth_url,
            data=self.get_credential(),
            headers={
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'text/plain; charset=utf-8'
            },
            verify=self.verify_ssl
        )
        if self.session_request.status_code == 200:
            self.auth_token = self.session_request.text
        else:
            error = ''
            try:
                error = self.session_request.json()['error']
            except:
                error = self.session_request.text
            raise OATException(error)
        return s

 
    def get_json(self, api, use_cache=True):
        '''
        Like a normal 'get' but will use cache by
        default and return the json object from the request.
        '''
        d = None
        if use_cache:
            d = self.cache.get(api)
            if d is None:
                r = self.get(api)
                try:
                    d = r.json()
                except Exception as ex:
                    raise Exception(
                        'Issue with data: "{0}" -- Error: {1}'.format(
                            r.text, str(ex)
                        )
                    )
                self.cache[api] = d
        else:
            r = self.get(api)
            d = r.json()
        return d

    def get(self, api):
        '''
        Preform a http get. Recreate session if needed
        '''
        r = None
        url = self.api_url + api
        for retry in range(self.max_retries):
            if not self.session:
                self.session = self._create_session()
            r = self.session.get(url, headers={
                'Accept': 'application/json',
                'Content-Type': 'text/plain; charset=utf-8',
                'Authorization': 'Bearer ' + self.auth_token
                })
            if r.status_code == 200:
                return r
            if r.status_code == 404:
                # record lookup successfuly found nothing
                return r
            # perhaps session timed out and needs to be recreated
            self.session = None
            # short sleep to prevent flooding
            time.sleep(0.2)
        raise Exception(
            'Hit max retries {0}: Error: {1}'.format(
                self.max_retries,
                r.text
            )
        )
