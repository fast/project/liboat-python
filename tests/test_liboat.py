from unittest import TestCase
import os

import liboat

class TestConnection(TestCase):
    def setUp(self):
        self.user = os.environ['LIBOAT_USER']
        self.key = os.environ['LIBOAT_KEY']
        self.oat = liboat.OATSession(
            api_user=self.user,
            api_key=self.key
        )

    def test_basic_connection(self):
        separate_session = liboat.OATSession(
            api_user=self.user,
            api_key=self.key
        )
        separate_session = self.oat._create_session()
    
    def test_user_lookup(self):
        lookup = self.oat.user_lookup(
            query='vucic',
            include_students=True,
            include_staff=True
        )
        self.assertIsInstance(lookup, list)
        self.assertNotEqual(len(lookup), 0)