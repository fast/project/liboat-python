# Python Client for LibOAT
This is a direct copy of the main python OAT library found at [oat client](https://git.uwaterloo.ca/oat/oat-client), wrapped with a `setup.py` and some optional helper functions.

>*Note that a username and private/public keypair will be required to authenticate against the OAT webapi. Approval must be obtained by contacting the OAT team with use case and requested [endpoints](https://oat.uwaterloo.ca/api/v2/).*

## Installation
You'll need to install from gitlab using the `git+` pip option. This means you can pin *specific* versions (eg. `1.0`), but can't use constraints (eg. `1.*` or `>=1.*` won't work)


You can also omit the @VERSION to install from master (Not recommended).

```bash
# pip install or within a requirements.txt:
git+https://git.uwaterloo.ca/fast/project/liboat-python.git@VERSION#egg=liboat

# in a setup.py install_requires:
liboat@git+https://git.uwaterloo.ca/fast/project/liboat-python.git@VERSION
```

For the latest value of `VERSION` check the tags on this repo.


## Using the helper functions
To make using the library in python a tiny bit more user friendly, most of the documented functions listed [on the OAT api documentation](https://oat.uwaterloo.ca/api/v2/) have been mapped to member functions of the OATSession class.

*OATSession is a subclass of the standard libOAT Session which includes 

```python
from liboat import OATSession
oat = OATSession(api_user=XX, api_key=XXX)

# Using helper functions
classlist = oat.classlist(
    1211,
    -12001,
    mode='sid'
)

# Equivalent classic get method
classlist = oat.get('/api/v2/course/1211/-12001.sid/classlist').json()
```

Read `liboat/oat_session.py` to see the local naming convention of various endpoints, as they're not always 1-to-1.


## Using LibOAT Directly
If you want to use the normal libOAT library (without the helper functions), it's accessible via liboat.session:
```python
from liboat.session import Session
...
```

## Testing
To test everything install tox (I know, overkill) and then:

```bash
# Assuming your private key is in key.pem
export LIBOAT_KEY=`cat key.pem`
export LIBOAT_USER=_youruser
tox
```

**At the moment we don't test many of the helper functions**. This is because most keys won't have access to all of the endpoints. In the future it may be good to set up a way to test a specific set of helper functions for project-specific testing.

(more tests are needed in general)
