from urllib.parse import urlencode
from .session import Session
from enum import Enum
from typing import Union, List, Dict, Any
from typing_extensions import Literal

CourseSearchModes = Literal['crs', 'cid', 'sid', 'str']
OfferingModes = Literal['crs', 'sid', 'str', 'all']
SearchType = Union[str, int]
UWID = int
UserID = str
UserOrUW = Union[UWID, UserID]

# Vague Response Types
DictResponse = Dict[str, Any]
ListResponse = List[DictResponse]


class OATSession(Session):
    # Class Listing Stuff =========================================================
    def classlist(self, termid: int, search: SearchType, mode: CourseSearchModes='crs') -> DictResponse:
        if mode in ['crs', 'cid', 'sid']:
            return self.get(f'/api/v2/course/{termid}/{search}.{mode}/classlist').json()["data"]
        elif mode == 'str':
            return self.get(f'/api/v2/course/{termid}/{search}/classlist').json()["data"]
        raise Exception(f'Invalid mode. Must match {CourseSearchModes}')

    def offerings(self, termid: int, search: Union[None, SearchType], mode: OfferingModes='crs') -> Union[DictResponse, ListResponse]:
        if mode in ['crs', 'sid']:
            if search is None:
                raise Exception('Must provide search term')
            return self.get(f'/api/v2/course/{termid}/{search}.{mode}/offering').json()["data"]
        elif mode == 'str':
            return self.get(f'/api/v2/course/{termid}/{search}/offerings').json()["data"]
        elif mode == 'all':
            return self.get(f'/api/v2/course/{termid}/offerings').json()["data"]
        raise Exception(f'Invalid mode. Must match {OfferingModes}')

    def offerings_withsections(self, termid: int) -> ListResponse:
        return self.get(f'/api/v2/course/{termid}/offerings-withSections').json()["data"]

    # Student Information ======================================================
    def student_courses(self, termid: int, id: UserID) -> ListResponse:
        return self.get(f'/api/v2/course/{id}/{termid}/courses').json()["data"]
    def student_awards(self, id: UWID) -> ListResponse:
        return self.get(f'/api/v2/student/{id}/awards').json()["data"]
    def student_bio_limited(self, id: UserOrUW) -> ListResponse:
        return self.get(f'/api/v2/student/{id}/bio.limited').json()["data"]
    def student_coop(self, id: UWID) -> ListResponse:
        return self.get(f'/api/v2/student/{id}/coop').json()["data"]
    def student_coursedetails(self, id: UserOrUW) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/courseDetails').json()["data"]
    def student_coursedetails_specific(self, id: UserOrUW, termid: int, courseid: int) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/courseDetails/{termid}/{courseid}.crs').json()["data"]
    def student_grades(self, id: UserOrUW) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/courseGrades').json()["data"]
    def student_audit(self, id: UWID) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/degreeAudit').json()["data"]
    def student_recentterm(self, id: UserOrUW) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/termPlansRecent').json()["data"]
    def student_progplan(self, id: UserOrUW) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/progPlanByTerm').json()["data"]
    def student_termsummary(self, id: UserOrUW, termid: int) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/{termid}/termSummary').json()["data"]
    
    # Plan Information ===========================================================
    def program_codes(self, regex: str) -> ListResponse:
        return self.get(f'/api/v2/pps/program/{regex}').json()["data"]
    def plangroup_tree(self, regex: str) -> DictResponse:
        return self.get(f'/api/v2/pps/planGroupTree/{regex}').json()["data"]
    def plangroups(self) -> ListResponse:
        return self.get(f'/api/v2/pps/planGroups').json()["data"]
    def plans(self, regex: str) -> ListResponse:
        return self.get(f'/api/v2/pps/plan/{regex}').json()["data"]
    def plangroup_students(self, termid: int, plangroup: str) -> ListResponse:
        return self.get(f'/api/v2/students/{plangroup}/{termid}').json()["data"]
    
    # Grad Stuff ===============================================================
    def grad_students(self, id: UserID, termid: int) -> DictResponse:
        return self.get(f'/api/v2/faculty/{id}/{termid}/gradStudents').json()["data"]
    def grad_overview(self, id: UWID, termid: int) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/{termid}/gradOverview').json()["data"]
    def grad_supervisors(self, id: UserID, termid: int) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/{termid}/gradSupervisors').json()["data"]
    
    # Society Stuff =============================================================
    def society_membercount(self, group: str, termid: int) -> DictResponse:
        return self.get(f'/api/v2/student/stats/societyMembershipCount/{group}.{termid}').json()["data"]
    def society_member(self, id: UserID, group: str, termid: int) -> DictResponse:
        return self.get(f'/api/v2/student/{id}/societyMember/{group}.{termid}').json()["data"]

    # General Information ======================================================
    def watiam(self, userid: UserID) -> DictResponse:
        return self.get(f'/api/v2/person/{userid}/watiam').json()["data"]
    def user_lookup(self, query: Union[str, int], num:int=10, include_students:bool=True, include_staff:bool=False):
        q = urlencode(dict(query=query, num=num, include_staff=include_staff, include_students=include_students))
        return self.get(f'/api/v2/util/searchSuggestions?{q.lower()}').json()["data"]

        
